(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'main': '/'
    }
  });

  var proxyCustomElements = function proxyCustomElements() {
    var _customElementsDefine = window.customElements.define;

    window.customElements.define = (name, cl, conf) => {
      if (!customElements.get(name)) {
        _customElementsDefine.call(window.customElements, name, cl, conf);
      } else {
        console.warn(`${name} has been defined twice`);
      }
    };
  };

  proxyCustomElements();
}(document));
