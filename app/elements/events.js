module.exports = () => {
  return {
    name: {
      get: 'get-name',
      set: 'set-name',
      getLoaded: 'get-name-loaded'
    }
  }
};