{
  class MainPage extends Polymer.mixinBehaviors([CellsBehaviors.i18nBehavior] , Polymer.Element) {
    constructor() {
      super();
    }

    static get is() {
      return 'main-page';
    }

    onGetName(ev) {
      this.dispatchEvent(new CustomEvent('get-name', {
        bubbles: true,
        composed: true
      }));
    }

    printName(data) {
      console.log(data);
    }
  }
  customElements.define(MainPage.is, MainPage);
}

