{
  'use strict';

  module.exports = config => {

    const page = 'main';
    const template = require('./commons/template')();
    const events = require('../elements/events')();
    const channels = require('./commons/channels')();

    const pageTemplate = {
      zone: "app__main",
      type: "UI",
      familyPath: `../elements/pages/${page}-page`,
      tag: `${page}-page`,
      properties: {
        cellsConnections: {
          in: {
            [`${channels.name.getLoaded}`]: {
              bind: 'printName'
            }
          },
          out: {
            [`${channels.name.get}`]: {
              bind: events.name.get
            }
          }
        }
      }
    };


    const mainDM = {
      zone: "app__main",
      type: 'CC',
      familyPath: `../elements/data-managers/${page}-dm`,
      tag: `${page}-dm`,
      properties: {
        cellsConnections: {
          in: {
            [`${channels.name.get}`]: {
              bind: 'getName'
            }
          },
          out: {
            [`${channels.name.getLoaded}`]: {
              bind: events.name.getLoaded
            }
          }
        }
      }
    };

     return {
       template,
       components: [
         pageTemplate,
         mainDM
       ]
     }
  }
}